package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
// 2039039 nicholas Pouliezos
/**
 * Unit test for simple App.
 */
public class AppTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void echo(){
        App app =new App();
        assertEquals( "makeing sure its true",5, app.echo(5));
    }
    @Test
    public void oneMore(){
        App app =new App();
        assertEquals( "makeing sure its true by adding 1 to the x",6, app.oneMore(5));
    }
}
